package eu.catalyst.marketsession.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import eu.catalyst.marketsession.model.MarketAction;
import eu.catalyst.marketsession.model.MarketActionCounterOffer;
import eu.catalyst.marketsession.global.MyPayload;;

@Stateless
@Path("/marketplace/{marketId}/form/{form}/marketsessions/{sessionId}/actions/sorted/")
public class ClearingActions {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ClearingActions.class);

	static HashSet<String> setOfFormsToClear = new HashSet<>();

	static {
		setOfFormsToClear.add("electric_energy");
		setOfFormsToClear.add("thermal_energy");
		setOfFormsToClear.add("it_load");
	}

	public ClearingActions() {
	}

	@POST
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })
	public List<MarketAction> postListActionsToClearing(@PathParam("marketId") int marketId, @PathParam("form") String form,
			@PathParam("sessionId") int sessionId, List<MarketAction> actions) {

		System.out.println("postListActionsToClearing");
		

		if (setOfFormsToClear.contains(form)) {

				try {
					Properties prop = new Properties();

					prop.load(this.getClass().getClassLoader().getResourceAsStream("config.properties"));

					// Check the property informationBrokerServerUrl in file config.properties

					String marketSessionManagerUrl = prop.getProperty("marketClearingManagerUrl");

					URL url = new URL(marketSessionManagerUrl + "/marketplace/" + marketId
							+ "/form/" + form + "/marketsessions/" + sessionId + "/actions/sorted/");

					System.out.println("Rest url: " + url.toString());

					HttpURLConnection connService = (HttpURLConnection) url.openConnection();
					connService.setDoOutput(true);
					connService.setRequestMethod("POST");
					connService.setRequestProperty("Accept", "application/json");
					connService.setRequestProperty("Content-Type", "application/json");
					GsonBuilder gb = new GsonBuilder();

					// gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
					gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
					Gson gson = gb.create();

					String record = gson.toJson(actions);

					System.out.println("Rest Request: " + record);

					OutputStream os = connService.getOutputStream();
					os.write(record.getBytes());

					int myResponseCode = connService.getResponseCode();
					System.out.println("HTTP error code :" + myResponseCode);
					
					ArrayList<MarketAction> returnActions = new ArrayList<MarketAction>();
					
					BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
					Type listType = new TypeToken<ArrayList<MarketAction>>() {
					}.getType();
					gb = new GsonBuilder();
					gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
					gson = gb.create();
					// returnMarketActionCounterOfferList = new Gson().fromJson(br, listType);
					returnActions = gson.fromJson(br, listType);
					//force actions to return returnActions
					actions = returnActions;
					os.flush();
					os.close();
					br.close();
					connService.disconnect();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		return actions;
	}

	static class myMarketAction {
		MarketAction marketAction;
		// long residualValue;
		BigDecimal residualValue;

		public myMarketAction() {
		}
	}

}


