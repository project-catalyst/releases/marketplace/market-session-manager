package eu.catalyst.marketsession.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import eu.catalyst.marketsession.model.*;
import eu.catalyst.marketsession.utilities.Utilities;
import eu.catalyst.marketsession.global.MyPayload;

import javax.ejb.Stateless;
import javax.servlet.ServletContextEvent;
import javax.ws.rs.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import eu.catalyst.marketsession.model.MarketActionCounterOffer;
import java.io.StringReader;

@Stateless
@Path("/marketplace/{marketId}/form/{form}/marketsessions/{sessionId}/billing/actions/")
public class BillingActions {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(BillingActions.class);

	static HashSet<String> setOfFormsEnergyMarket = new HashSet<>();

	static {
		setOfFormsEnergyMarket.add("electric_energy");
		setOfFormsEnergyMarket.add("thermal_energy");
		setOfFormsEnergyMarket.add("it_load");
	}

	public BillingActions() {
	}

	@POST
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })
	public void postListActionsToBilling(@PathParam("marketId") int marketId, @PathParam("form") String form,
			@PathParam("sessionId") int sessionId, MyPayload myPayload) {

		System.out.println("postListActions");

		if (setOfFormsEnergyMarket.contains(form)) {
			BillingActionsEnergyMarket(marketId, form, sessionId, myPayload);
		} else {
			BillingActionsFlexMarket(marketId, form, sessionId, myPayload);
		}

	}

	
	public void BillingActionsEnergyMarket(int marketId, String form, int sessionId, MyPayload myPayload) {
		if (!myPayload.getBidsOffersActionList().isEmpty()) {

			try {
				Properties prop = new Properties();

				prop.load(this.getClass().getClassLoader().getResourceAsStream("config.properties"));

				// Check the property informationBrokerServerUrl in file config.properties

				String informationBrokerServerUrl = prop.getProperty("informationBrokerServerUrl");

				URL url = new URL(informationBrokerServerUrl + "/marketplace/" + marketId + "/marketsessions/"
						+ sessionId + "/clearing/actions/");

				System.out.println("Rest url: " + url.toString());

				HttpURLConnection connService = (HttpURLConnection) url.openConnection();
				connService.setDoOutput(true);
				connService.setRequestMethod("POST");
				connService.setRequestProperty("Accept", "application/json");
				connService.setRequestProperty("Content-Type", "application/json");
				log.debug("Authorization: " + Utilities.TOKEN_AUTHORIZATION);
				connService.setRequestProperty("Authorization", Utilities.TOKEN_AUTHORIZATION);
				GsonBuilder gb = new GsonBuilder();

				// gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
				gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
				Gson gson = gb.create();

				String record = gson.toJson(myPayload.getBidsOffersActionList());

				System.out.println("Rest Request: " + record);

				OutputStream os = connService.getOutputStream();
				os.write(record.getBytes());

				int myResponseCode = connService.getResponseCode();
				System.out.println("HTTP error code :" + myResponseCode);

				os.flush();
				os.close();
				connService.disconnect();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		// Invoke Rest Service (POST) "/marketplace/{marketId}/counteroffers" of IB
		// passing marketActionCounterOfferList

		ArrayList<MarketActionCounterOffer> returnMarketActionCounterOfferList = new ArrayList<MarketActionCounterOffer>();

		if (!myPayload.getMarketActionCounterOfferList().isEmpty()) {

			try {
				Properties prop = new Properties();

				prop.load(this.getClass().getClassLoader().getResourceAsStream("config.properties"));

				// Check the property informationBrokerServerUrl in file config.properties

				String informationBrokerServerUrl = prop.getProperty("informationBrokerServerUrl");

				URL url = new URL(informationBrokerServerUrl + "/marketplace/" + marketId + "/counteroffers/");

//for test   	URL url = new URL(informationBrokerServerUrl + "/marketplace/" + marketId + "/marketsessions/" + sessionId + "/actions/counteroffers");

				System.out.println("Rest url: " + url.toString());

				HttpURLConnection connService = (HttpURLConnection) url.openConnection();
				connService.setDoOutput(true);
				connService.setRequestMethod("POST");
				connService.setRequestProperty("Accept", "application/json");
				connService.setRequestProperty("Content-Type", "application/json");
				log.debug("Authorization: " + Utilities.TOKEN_AUTHORIZATION);
				connService.setRequestProperty("Authorization", Utilities.TOKEN_AUTHORIZATION);
				GsonBuilder gb = new GsonBuilder();

				// gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
				gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
				Gson gson = gb.create();

				String record = gson.toJson(myPayload.getMarketActionCounterOfferList());

				System.out.println("Rest Request: " + record);

				OutputStream os = connService.getOutputStream();
				os.write(record.getBytes());

				int myResponseCode = connService.getResponseCode();
				System.out.println("HTTP error code :" + myResponseCode);

//test			if (connService.getResponseCode() != HttpURLConnection.HTTP_OK) {
//					throw new RuntimeException("Failed : HTTP error code : " + connService.getResponseCode());
//
//				}

				BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
				Type listType = new TypeToken<ArrayList<MarketActionCounterOffer>>() {
				}.getType();
				gb = new GsonBuilder();
				gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
				gson = gb.create();
				// returnMarketActionCounterOfferList = new Gson().fromJson(br, listType);
				returnMarketActionCounterOfferList = gson.fromJson(br, listType);
				os.flush();
				os.close();
				br.close();
				connService.disconnect();

			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		// create new list of Transaction
		// iterator on marketActionCounterOfferList
		// for each item:
		// generate Transaction object
		// set datetime with current date
		// set marketActionCounterOfferid with current item of Iterator
		// add Transaction object to list of Transaction
		// Invoke Rest Service (POST)
		// "/marketplace/{marketId}/marketSessions/{sessionId}/transactions" of IB using
		// list of Transaction

		ArrayList<Transaction> transactionList = new ArrayList<Transaction>();
		ArrayList<Transaction> returnTransactionList = new ArrayList<Transaction>();
		// for (Iterator<MarketActionCounterOffer> iterator =
		// marketActionCounterOfferList.iterator(); iterator.hasNext();) {
		for (Iterator<MarketActionCounterOffer> iterator = returnMarketActionCounterOfferList.iterator(); iterator
				.hasNext();) {
			MarketActionCounterOffer myMarketActionCounterOffer = iterator.next();
			Transaction myTransaction = new Transaction();
			myTransaction.setDateTime(new Date());
//			myTransaction.setMarketActionCounterOfferid(myMarketActionCounterOffer);
			myTransaction.setMarketActionCounterOfferid(myMarketActionCounterOffer.getId());
			transactionList.add(myTransaction);
		}

		if (!transactionList.isEmpty()) {
			try {
				Properties prop = new Properties();

				prop.load(this.getClass().getClassLoader().getResourceAsStream("config.properties"));

				// Check the property informationBrokerServerUrl in file config.properties

				String informationBrokerServerUrl = prop.getProperty("informationBrokerServerUrl");

				URL url = new URL(informationBrokerServerUrl + "/marketplace/" + marketId + "/marketsessions/"
						+ sessionId + "/transactions/");

				System.out.println("Rest url: " + url.toString());

				HttpURLConnection connService = (HttpURLConnection) url.openConnection();
				connService.setDoOutput(true);
				connService.setRequestMethod("POST");
				connService.setRequestProperty("Accept", "application/json");
				connService.setRequestProperty("Content-Type", "application/json");
				log.debug("Authorization: " + Utilities.TOKEN_AUTHORIZATION);
				connService.setRequestProperty("Authorization", Utilities.TOKEN_AUTHORIZATION);
				GsonBuilder gb = new GsonBuilder();

				// gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
				gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
				Gson gson = gb.create();

				String record = gson.toJson(transactionList);

				System.out.println("Rest Request: " + record);

				OutputStream os = connService.getOutputStream();
				os.write(record.getBytes());

				int myResponseCode = connService.getResponseCode();
				System.out.println("HTTP error code :" + myResponseCode);

//test          if (connService.getResponseCode() != HttpURLConnection.HTTP_OK) {
//					throw new RuntimeException("Failed : HTTP error code : " + connService.getResponseCode());
//				}

				BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
				Type listType = new TypeToken<ArrayList<Transaction>>() {
				}.getType();
				gb = new GsonBuilder();
				gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
				gson = gb.create();
				// returnTransactionList = new Gson().fromJson(br, listType);
				returnTransactionList = gson.fromJson(br, listType);

				os.flush();
				os.close();
				br.close();
				connService.disconnect();

			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		// Invoke Rest Service (PUT) "marketplace/{marketId}/marketsessions" of IB
		// passing market.getMarketSessionid() wrapped as ARRAYLIST
		// getting the first MarketSession of bidsOffersActionList
		
		
	
		//MarketSession myMarketSession = null;
		if ((myPayload.getNewClearingPrice() != null) && !myPayload.getBidsOffersActionList().isEmpty()) {
			MarketSession myMarketSession = null;
			try {
				myMarketSession = new Utilities().invokeGetMarketSession(myPayload.getBidsOffersActionList().get(0).getMarketSessionid().intValue());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
//			myMarketSession = myPayload.getBidsOffersActionList().get(0).getMarketSessionid();
			myMarketSession.setClearingPrice(myPayload.getNewClearingPrice());

			try {
				Properties prop = new Properties();

				prop.load(this.getClass().getClassLoader().getResourceAsStream("config.properties"));

				// Check the property informationBrokerServerUrl in file config.properties

				String informationBrokerServerUrl = prop.getProperty("informationBrokerServerUrl");

				URL url = new URL(informationBrokerServerUrl + "/marketplace/" + marketId + "/marketsessions/" + myMarketSession.getId() + "/");

				System.out.println("Rest url: " + url.toString());

				HttpURLConnection connService = (HttpURLConnection) url.openConnection();
				connService.setDoOutput(true);
				connService.setRequestMethod("PUT");
				connService.setRequestProperty("Accept", "application/json");
				connService.setRequestProperty("Content-Type", "application/json");
				log.debug("Authorization: " + Utilities.TOKEN_AUTHORIZATION);
				connService.setRequestProperty("Authorization", Utilities.TOKEN_AUTHORIZATION);
				GsonBuilder gb = new GsonBuilder();

				// gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
				gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
				Gson gson = gb.create();

				String record = gson.toJson(myMarketSession);

				System.out.println("Rest Request: " + record);

				OutputStream os = connService.getOutputStream();
				os.write(record.getBytes());

				int myResponseCode = connService.getResponseCode();
				System.out.println("HTTP error code :" + myResponseCode);
				os.flush();
				os.close();
				connService.disconnect();

			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		// create new list of Invoice
		// iterator on transactionList
		// for each item:
		// generate Invoice object
		// set datetime with current date
		// set transactionid with current item of Iterator
		// add Invoice object to list of Invoice
		// Invoke Rest Service (POST)
		// "/marketplace/{marketId}/marketSessions/{sessionId}/invoices" of IB using
		// list of Transaction

		ArrayList<Invoice> returnInvoiceList = new ArrayList<Invoice>();
		ArrayList<Invoice> invoiceList = new ArrayList<Invoice>();
		// for (Iterator<Transaction> iterator = transactionList.iterator();
		// iterator.hasNext();) {
		for (Iterator<Transaction> iterator = returnTransactionList.iterator(); iterator.hasNext();) {
			Transaction myTransaction = iterator.next();
			Invoice myInvoice = new Invoice();
			myInvoice.setDate(new Date());
			// BigDecimal myamount = new
			// BigDecimal(myTransaction.getMarketActionCounterOfferid().getExchangedValue());
			
			MarketActionCounterOffer myMarketActionCounterOffer = null;
			try {
				myMarketActionCounterOffer = new Utilities().invokeGetMarketActionCounterOffer(myTransaction.getMarketActionCounterOfferid().intValue());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			
//			BigDecimal myamount = myTransaction.getMarketActionCounterOfferid().getExchangedValue()
//					.multiply(myPayload.getNewClearingPrice());
			BigDecimal myamount = myMarketActionCounterOffer.getExchangedValue().multiply(myPayload.getNewClearingPrice());
			myInvoice.setPenalty(new BigDecimal(0));
			myInvoice.setFixedFee(new BigDecimal(0));
			myInvoice.setAmount(myamount);
			myInvoice.setTransactionid(myTransaction.getId());
			invoiceList.add(myInvoice);
		}

		if (!invoiceList.isEmpty()) {

			try {
				Properties prop = new Properties();

				prop.load(this.getClass().getClassLoader().getResourceAsStream("config.properties"));

				// Check the property informationBrokerServerUrl in file config.properties

				String informationBrokerServerUrl = prop.getProperty("informationBrokerServerUrl");

				URL url = new URL(informationBrokerServerUrl + "/marketplace/" + marketId + "/marketsessions/"
						+ sessionId + "/invoices/");

				System.out.println("Rest url: " + url.toString());

				HttpURLConnection connService = (HttpURLConnection) url.openConnection();
				connService.setDoOutput(true);
				connService.setRequestMethod("POST");
				connService.setRequestProperty("Accept", "application/json");
				connService.setRequestProperty("Content-Type", "application/json");
				log.debug("Authorization: " + Utilities.TOKEN_AUTHORIZATION);
				connService.setRequestProperty("Authorization", Utilities.TOKEN_AUTHORIZATION);
				GsonBuilder gb = new GsonBuilder();

				// gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
				gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
				Gson gson = gb.create();

				String record = gson.toJson(invoiceList);

				System.out.println("Rest Request: " + record);

				OutputStream os = connService.getOutputStream();
				os.write(record.getBytes());

				int myResponseCode = connService.getResponseCode();
				System.out.println("HTTP error code :" + myResponseCode);

//test   		if (connService.getResponseCode() != HttpURLConnection.HTTP_OK) {
//					throw new RuntimeException("Failed : HTTP error code : " + connService.getResponseCode());
//				}

				BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
				Type listType = new TypeToken<ArrayList<Invoice>>() {
				}.getType();
				gb = new GsonBuilder();
				gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
				gson = gb.create();
				// returnInvoiceList = new Gson().fromJson(br, listType);
				returnInvoiceList = gson.fromJson(br, listType);
				os.flush();
				os.close();
				br.close();
				connService.disconnect();

			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return;

	}


	public void BillingActionsFlexMarket(int marketId, String form, int sessionId, MyPayload myPayload) {
	
		Properties prop = new Properties();
		// load properties from the class path
		List<MarketAction> returnMarketActionNotBilledList;

		try {
			prop.load(this.getClass().getClassLoader().getResourceAsStream("config.properties"));

			String informationBrokerServerUrl = prop.getProperty("informationBrokerServerUrl");

			returnMarketActionNotBilledList = myPayload.getBidsOffersActionList();

			for (Iterator<MarketAction> iterator = returnMarketActionNotBilledList.iterator(); iterator.hasNext();) {

				List<MarketAction> marketActionBidList = new ArrayList<MarketAction>();

				MarketAction myBid = new MarketAction();
				MarketAction myOffer = iterator.next();

				myBid.setDate(myOffer.getDate());
				myBid.setActionStartTime(myOffer.getActionStartTime());
				myBid.setActionEndTime(myOffer.getActionEndTime());
				myBid.setValue(myOffer.getValue());
				myBid.setUom(myOffer.getUom());
				myBid.setPrice(myOffer.getPrice());
				myBid.setDeliveryPoint(myOffer.getDeliveryPoint());
				myBid.setMarketSessionid(myOffer.getMarketSessionid());
//				myBid.setMarketActorid(myOffer.getMarketSessionid().getMarketplaceid().getDSOid());
				MarketSession myMarketSession = new Utilities().invokeGetMarketSession(myOffer.getMarketSessionid().intValue());
				Marketplace myMarketplace = new Utilities().invokeGetMarketplace(myMarketSession.getMarketplaceid().intValue());
				myBid.setMarketActorid(myMarketplace.getDSOid().getId().intValue());
				
				myBid.setFormid(myOffer.getFormid());
//				myBid.setActionTypeid(new ActionType(1, "bid"));
// force actionType ID to 2 (actual Bid type ID)				
				myBid.setActionTypeid(2);
				myBid.setStatusid(myOffer.getStatusid());


				marketActionBidList.add(myBid);

				// create list(1 item) with current MarketAction
				// invoke POST
				// "/marketplace/{marketId}/marketsessions/{sessionId}/actions"
				// and retrieve list(1 item) of updated MarketAction

//				URL url2 = new URL(informationBrokerServerUrl + "/marketplace/"
//						+ myBid.getMarketSessionid().getMarketplaceid().getId().intValue() + "/marketsessions/"
//						+ myBid.getMarketSessionid().getId().intValue() + "/actions/");

				URL url2 = new URL(informationBrokerServerUrl + "/marketplace/"
				+ myMarketSession.getMarketplaceid().intValue() + "/marketsessions/"
				+ myMarketSession.getId().intValue() + "/actions/");

				
				log.debug("Start - Rest url2: " + url2.toString());
				HttpURLConnection connService2 = (HttpURLConnection) url2.openConnection();
				connService2.setDoOutput(true);
				connService2.setRequestMethod("POST");
				connService2.setRequestProperty("Content-Type", "application/json");
				connService2.setRequestProperty("Accept", "application/json");
				log.debug("Authorization: " + Utilities.TOKEN_AUTHORIZATION);
				connService2.setRequestProperty("Authorization", Utilities.TOKEN_AUTHORIZATION);

				GsonBuilder gb2 = new GsonBuilder();
				gb2.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
				Gson gson2 = gb2.create();
				String record2 = gson2.toJson(marketActionBidList);
				log.debug("Rest Request: " + record2);

				OutputStream os2 = connService2.getOutputStream();
				os2.write(record2.getBytes());

				int myResponseCode2 = connService2.getResponseCode();
				log.debug("HTTP error code :" + myResponseCode2);
//test			if (myResponseCode2 != HttpURLConnection.HTTP_OK && myResponseCode2 != HttpURLConnection.HTTP_CREATED) {
//					throw new RuntimeException("Failed : HTTP error code : " + connService2.getResponseCode());
//				}

     		BufferedReader br2 = new BufferedReader(new InputStreamReader((connService2.getInputStream())));
				Type listType2 = new TypeToken<ArrayList<MarketAction>>() {
				}.getType();
				gb2 = new GsonBuilder();
				gb2.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
				gson2 = gb2.create();
				// GSONBUILDER GB2 = NEW GSONBUILDER();
				// GB2.SETDATEFORMAT("YYYY-MM-DD'T'HH:MM:SSX");
				// GSON GSON2 = GB.CREATE();
				ArrayList<MarketAction> returnMarketActionBidList = new ArrayList<MarketAction>();
			returnMarketActionBidList = gson2.fromJson(br2, listType2);

//---------------------
//				String test = "[{\"id\":21,\"date\":\"2019-05-14T00:00:00Z\",\"actionStartTime\":\"2019-05-14T11:00:00Z\",\"actionEndTime\":\"2019-05-14T12:00:00Z\",\"value\":\"10.000\",\"uom\":\"kW\",\"price\":\"10.000\",\"deliveryPoint\":\"My Custom Delivery Point\",\"marketSessionid\":1,\"marketActorid\":3,\"formid\":3,\"actionTypeid\":1,\"statusid\":2,\"loadid\":null}]";
//				StringReader inputString = new StringReader(test);
//				BufferedReader br99 = new BufferedReader(inputString);
//				returnMarketActionBidList = gson2.fromJson(br99, listType2);
//---------------------
				
				
				os2.flush();
				os2.close();
				br2.close();
				connService2.disconnect();

				// Create list(1 item) with MarketActionCounterOffer with
				// current bid and offer and offer value
				// invoke POST "/marketplace/{marketId}/counteroffers" and
				// retrieve list(1 item) of updated MarketActionCounterOffer

				ArrayList<MarketActionCounterOffer> marketActionCounterOfferList = new ArrayList<MarketActionCounterOffer>();

				MarketActionCounterOffer myMarketActionCounterOffer = new MarketActionCounterOffer();
//				myMarketActionCounterOffer.setMarketAction_Bid_id(returnMarketActionBidList.get(0));
//				myMarketActionCounterOffer.setMarketAction_Offer_id(myOffer);
				myMarketActionCounterOffer.setMarketAction_Bid_id(returnMarketActionBidList.get(0).getId().intValue());
				myMarketActionCounterOffer.setMarketAction_Offer_id(myOffer.getId().intValue());

				
				myMarketActionCounterOffer.setExchangedValue(myOffer.getValue());
				marketActionCounterOfferList.add(myMarketActionCounterOffer);

//				URL url3 = new URL(informationBrokerServerUrl + "/marketplace/"
//						+ myBid.getMarketSessionid().getMarketplaceid().getId().intValue() + "/counteroffers/");
				URL url3 = new URL(informationBrokerServerUrl + "/marketplace/"
						+ myMarketSession.getMarketplaceid().intValue() + "/counteroffers/");
				
				
				
				// only for test
				// URL url3 = new URL(informationBrokerServerUrl +
				// "/marketplace/"
				// +
				// myBid.getMarketSessionid().getMarketplaceid().getId().intValue()
				// + "/marketsessions/"+
				// myBid.getMarketSessionid().getId().intValue() +
				// "/actions/counteroffers");

				log.debug("Start - Rest url3: " + url3.toString());
				HttpURLConnection connService3 = (HttpURLConnection) url3.openConnection();
				connService3.setDoOutput(true);
				connService3.setRequestMethod("POST");
				connService3.setRequestProperty("Content-Type", "application/json");
				connService3.setRequestProperty("Accept", "application/json");
				log.debug("Authorization: " + Utilities.TOKEN_AUTHORIZATION);
				connService3.setRequestProperty("Authorization", Utilities.TOKEN_AUTHORIZATION);

				GsonBuilder gb3 = new GsonBuilder();
				gb3.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
				Gson gson3 = gb3.create();
				String record3 = gson3.toJson(marketActionCounterOfferList);
				log.debug("Rest Request: " + record3);

				OutputStream os3 = connService3.getOutputStream();
				os3.write(record3.getBytes());

				int myResponseCode3 = connService3.getResponseCode();
				log.debug("HTTP error code :" + myResponseCode3);
//test			if (myResponseCode3 != HttpURLConnection.HTTP_OK
//						&& myResponseCode3 != HttpURLConnection.HTTP_NO_CONTENT) {
//					throw new RuntimeException("Failed : HTTP error code : " + connService3.getResponseCode());
//				}

				BufferedReader br3 = new BufferedReader(new InputStreamReader((connService3.getInputStream())));
				Type listType3 = new TypeToken<ArrayList<MarketActionCounterOffer>>() {
				}.getType();
				// GSONBUILDER GB2 = NEW GSONBUILDER();
				// GB2.SETDATEFORMAT("YYYY-MM-DD'T'HH:MM:SSX");
				// GSON GSON2 = GB.CREATE();
				ArrayList<MarketActionCounterOffer> returnMarketActionCounterOfferList = new ArrayList<MarketActionCounterOffer>();
				returnMarketActionCounterOfferList = gson3.fromJson(br3, listType3);

				os3.flush();
				os3.close();
				br3.close();
				connService3.disconnect();

				// Create list(1 item) with Transaction with current
				// MarketActionCounterOffer and current date
				// invoke POST
				// "/marketplace/{marketId}/marketsessions/{sessionId}/transactions"
				// and retrieve list(1 item) of updated Transaction

				ArrayList<Transaction> transactionList = new ArrayList<Transaction>();

				Transaction myTransaction = new Transaction();
				myTransaction.setDateTime(new Date());
//				myTransaction.setMarketActionCounterOfferid(returnMarketActionCounterOfferList.get(0));
				myTransaction.setMarketActionCounterOfferid(returnMarketActionCounterOfferList.get(0).getId());
				transactionList.add(myTransaction);

//				URL url4 = new URL(informationBrokerServerUrl + "/marketplace/"
//						+ myBid.getMarketSessionid().getMarketplaceid().getId().intValue() + "/marketsessions/"
//						+ myBid.getMarketSessionid().getId().intValue() + "/transactions/");
				URL url4 = new URL(informationBrokerServerUrl + "/marketplace/"
				+ myMarketSession.getMarketplaceid().intValue() + "/marketsessions/"
				+ myMarketSession.getId().intValue() + "/transactions/");
				
				
				// only for test + myBid.getMarketSessionid().getId().intValue()
				// + "/actions/transactions");

				log.debug("Start - Rest url4: " + url4.toString());
				HttpURLConnection connService4 = (HttpURLConnection) url4.openConnection();
				connService4.setDoOutput(true);
				connService4.setRequestMethod("POST");
				connService4.setRequestProperty("Content-Type", "application/json");
				connService4.setRequestProperty("Accept", "application/json");
				log.debug("Authorization: " + Utilities.TOKEN_AUTHORIZATION);
				connService4.setRequestProperty("Authorization", Utilities.TOKEN_AUTHORIZATION);

				GsonBuilder gb4 = new GsonBuilder();
				gb4.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
				Gson gson4 = gb4.create();
				String record4 = gson4.toJson(transactionList);
				log.debug("Rest Request: " + record4);

				OutputStream os4 = connService4.getOutputStream();
				os4.write(record4.getBytes());

				int myResponseCode4 = connService4.getResponseCode();
				log.debug("HTTP error code :" + myResponseCode4);
//test			if (myResponseCode4 != HttpURLConnection.HTTP_OK
//						&& myResponseCode4 != HttpURLConnection.HTTP_NO_CONTENT) {
//					throw new RuntimeException("Failed : HTTP error code : " + connService4.getResponseCode());
//				}

				BufferedReader br4 = new BufferedReader(new InputStreamReader((connService4.getInputStream())));
				Type listType4 = new TypeToken<ArrayList<Transaction>>() {
				}.getType();
				// GSONBUILDER GB2 = NEW GSONBUILDER();
				// GB2.SETDATEFORMAT("YYYY-MM-DD'T'HH:MM:SSX");
				// GSON GSON2 = GB.CREATE();
				ArrayList<Transaction> returnTransactionList = new ArrayList<Transaction>();
				returnTransactionList = gson4.fromJson(br4, listType4);

				os4.flush();
				os4.close();
				br4.close();
				connService4.disconnect();

				// Create list(1 item) with Invoice with current Transaction and
				// current date
				// set fixedFee field : property value FIXEDFEE always
				// set amount field : property value AMOUNT if ActionStatus is
				// "delivered"
				// set penalty field : property value PENALTY if ActionStatus is
				// "not_delivered"
				// invoke POST
				// "/marketplace/{marketId}/marketsessions/{sessionId}/invoices"
				// and retrieve list(1 item) of updated Invoice

				ArrayList<Invoice> invoiceList = new ArrayList<Invoice>();
				Invoice myInvoice = new Invoice();
				myInvoice.setDate(new Date());
//				myInvoice.setTransactionid(returnTransactionList.get(0));
				myInvoice.setTransactionid(returnTransactionList.get(0).getId());

				String propFixedFee = prop.getProperty("fixedFee");
				String propPenalty = prop.getProperty("penalty");
				// String propAmount = prop.getProperty("amount");

				myInvoice.setFixedFee(new BigDecimal(propFixedFee));
				myInvoice.setPenalty(new BigDecimal(0));
				myInvoice.setAmount(new BigDecimal(0));

//				if (myOffer.getActionStatusid().getId().intValue() == 13) { // not_delivered
//					myInvoice.setPenalty(new BigDecimal(propPenalty));
//				} else if (myOffer.getActionStatusid().getId().intValue() == 12) { // delivered
				if (myOffer.getStatusid().intValue() == 13) { // not_delivered
					myInvoice.setPenalty(new BigDecimal(propPenalty));
				} else if (myOffer.getStatusid().intValue() == 12) { // delivered
					// BigDecimal myamount = new
					// BigDecimal(myTransaction.getMarketActionCounterOfferid().getExchangedValue());
					// myamount =
					// myamount.multiply(myTransaction.getMarketActionCounterOfferid().getMarketAction_Offer_id().getPrice());
					
					MarketActionCounterOffer myMarketActionCounterOffer2 = new Utilities().invokeGetMarketActionCounterOffer(myTransaction.getMarketActionCounterOfferid().intValue());
					
//					BigDecimal myamount = myTransaction.getMarketActionCounterOfferid().getExchangedValue().multiply(
//							myTransaction.getMarketActionCounterOfferid().getMarketAction_Offer_id().getPrice());
					MarketAction myMarketAction = new Utilities().invokeGetMarketAction(myMarketActionCounterOffer2.getMarketAction_Offer_id());
					
					BigDecimal myamount = myMarketActionCounterOffer2.getExchangedValue().multiply(
							myMarketAction.getPrice());
					myInvoice.setAmount(myamount);
				}

				invoiceList.add(myInvoice);

//				URL url5 = new URL(informationBrokerServerUrl + "/marketplace/"
//						+ myBid.getMarketSessionid().getMarketplaceid().getId().intValue() + "/marketsessions/"
//						+ myBid.getMarketSessionid().getId().intValue() + "/invoices/");
				URL url5 = new URL(informationBrokerServerUrl + "/marketplace/"
						+ myMarketSession.getMarketplaceid().intValue() + "/marketsessions/"
						+ myMarketSession.getId().intValue() + "/invoices/");
				// only for test + myBid.getMarketSessionid().getId().intValue()
				// + "/actions/invoices");

				log.debug("Start - Rest url5: " + url5.toString());
				HttpURLConnection connService5 = (HttpURLConnection) url5.openConnection();
				connService5.setDoOutput(true);
				connService5.setRequestMethod("POST");
				connService5.setRequestProperty("Content-Type", "application/json");
				connService5.setRequestProperty("Accept", "application/json");
				log.debug("Authorization: " + Utilities.TOKEN_AUTHORIZATION);
				connService5.setRequestProperty("Authorization", Utilities.TOKEN_AUTHORIZATION);

				GsonBuilder gb5 = new GsonBuilder();
				gb5.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
				Gson gson5 = gb5.create();
				String record5 = gson5.toJson(invoiceList);
				log.debug("Rest Request: " + record5);

				OutputStream os5 = connService5.getOutputStream();
				os5.write(record5.getBytes());

				int myResponseCode5 = connService5.getResponseCode();
				log.debug("HTTP error code :" + myResponseCode5);
//test			if (myResponseCode5 != HttpURLConnection.HTTP_OK
//						&& myResponseCode5 != HttpURLConnection.HTTP_NO_CONTENT) {
//					throw new RuntimeException("Failed : HTTP error code : " + connService5.getResponseCode());
//				}

				BufferedReader br5 = new BufferedReader(new InputStreamReader((connService5.getInputStream())));
				Type listType5 = new TypeToken<ArrayList<Invoice>>() {
				}.getType();
				// GSONBUILDER GB2 = NEW GSONBUILDER();
				// GB2.SETDATEFORMAT("YYYY-MM-DD'T'HH:MM:SSX");
				// GSON GSON2 = GB.CREATE();
				ArrayList<Invoice> returnInvoiceList = new ArrayList<Invoice>();
				returnInvoiceList = gson5.fromJson(br5, listType5);

				os5.flush();
				os5.close();
				br5.close();
				connService5.disconnect();

				// update ActionStatusid of current bid and offer with "billed"
				// invoke PUT
				// "/marketplace/{marketId}/marketsessions/cleared/actions/billed"

				List<MarketAction> marketActionBilledList = new ArrayList<MarketAction>();

				MarketAction mybid2 = returnMarketActionBidList.get(0);
//				mybid2.setActionStatusid(new ActionStatus(13, "billed"));
				mybid2.setStatusid(14);
				marketActionBilledList.add(mybid2);

//				myOffer.setActionStatusid(new ActionStatus(13, "billed"));
				myOffer.setStatusid(14);
				marketActionBilledList.add(myOffer);

//				URL url6 = new URL(informationBrokerServerUrl + "/marketplace/"
//						+ myBid.getMarketSessionid().getMarketplaceid().getId().intValue()
//						+ "/marketsessions/cleared/actions/billed/");
				
				URL url6 = new URL(informationBrokerServerUrl + "/marketplace/"
						+ myMarketSession.getMarketplaceid().intValue()
						+ "/marketsessions/cleared/actions/billed/");

				log.debug("Start - Rest url6: " + url6.toString());
				HttpURLConnection connService6 = (HttpURLConnection) url6.openConnection();
				connService6.setDoOutput(true);
				connService6.setRequestMethod("PUT");
				connService6.setRequestProperty("Content-Type", "application/json");
				connService6.setRequestProperty("Accept", "application/json");
				log.debug("Authorization: " + Utilities.TOKEN_AUTHORIZATION);
				connService6.setRequestProperty("Authorization", Utilities.TOKEN_AUTHORIZATION);

				GsonBuilder gb6 = new GsonBuilder();
				gb6.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
				Gson gson6 = gb6.create();
				String record6 = gson6.toJson(marketActionBilledList);
				log.debug("Rest Request: " + record6);

				OutputStream os6 = connService6.getOutputStream();
				os6.write(record6.getBytes());

				int myResponseCode6 = connService6.getResponseCode();
				log.debug("HTTP error code :" + myResponseCode6);
//test			if (myResponseCode6 != HttpURLConnection.HTTP_OK
//						&& myResponseCode6 != HttpURLConnection.HTTP_NO_CONTENT) {
//					throw new RuntimeException("Failed : HTTP error code : " + connService6.getResponseCode());
//				}

				BufferedReader br6 = new BufferedReader(new InputStreamReader((connService6.getInputStream())));
				Type listType6 = new TypeToken<ArrayList<MarketAction>>() {
				}.getType();
				// GSONBUILDER GB2 = NEW GSONBUILDER();
				// GB2.SETDATEFORMAT("YYYY-MM-DD'T'HH:MM:SSX");
				// GSON GSON2 = GB.CREATE();
				ArrayList<MarketAction> returnMarketActionBilledList = new ArrayList<MarketAction>();
				returnMarketActionBilledList = gson6.fromJson(br6, listType6);

				os6.flush();
				os6.close();
				br6.close();
				connService6.disconnect();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}


}


