package eu.catalyst.marketsession.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import eu.catalyst.marketsession.model.ActionStatus;
import eu.catalyst.marketsession.model.Form;
import eu.catalyst.marketsession.model.MarketAction;
import eu.catalyst.marketsession.model.MarketSession;
import eu.catalyst.marketsession.utilities.Utilities;

@Stateless
@Path("/marketplace/{marketId}/marketsessions/{sessionId}/marketactions/")
public class CheckActions {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(CheckActions.class);

    public CheckActions() {
     
    }

    @POST
    @Consumes({"application/json", "application/xml"})
    @Produces({"application/json", "application/xml"})
    public List<MarketAction> postListActionsToCheck(
            @PathParam("marketId") int marketId,
            @PathParam("sessionId") int sessionId,
            List<MarketAction> actions) {

        boolean existActorInMarket = false;
        for (Iterator<MarketAction> iterator = actions.iterator(); iterator.hasNext(); ) {

            boolean validAction = false;
            boolean processAction = false;
            MarketAction market = iterator.next();    
            Date date = market.getDate();
            Date startDelivery = market.getActionStartTime();
            Date endDelivery = market.getActionEndTime();
            
			MarketSession myMarketSession = null;
			try {
				myMarketSession = new Utilities().invokeGetMarketSession(market.getMarketSessionid().intValue());
			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
            
//          Date startSessionDelivery = market.getMarketSessionid().getDeliveryStartTime();
			Date startSessionDelivery = myMarketSession.getDeliveryStartTime();
			
//          Date endSessionDelivery = market.getMarketSessionid().getDeliveryEndTime();
			Date endSessionDelivery = myMarketSession.getDeliveryEndTime();
			
//          Date startSession = market.getMarketSessionid().getSessionStartTime();
			Date startSession = myMarketSession.getSessionStartTime();
			
//          Date endSession = market.getMarketSessionid().getSessionEndTime();
			Date endSession = myMarketSession.getSessionEndTime();
			
//          int marketActorId = market.getMarketActorid().getId();
			int marketActorId = market.getMarketActorid().intValue();
			
            int marketplaceId = marketId;

            //Intermediate prototype updates
//          String marketActionForm = market.getFormid().getForm();
            Form myForm = null;
            Form myForm2 = null;
            String marketActionForm = null;
            String marketSessionForm = null;
            try {
				myForm = new Utilities().invokeGetForm(market.getFormid().intValue());
				marketActionForm = myForm.getForm();
	            myForm2  = new Utilities().invokeGetForm(myMarketSession.getFormid().intValue());
				marketSessionForm = myForm2.getForm();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
            
//          String marketSessionForm = market.getMarketSessionid().getFormid().getForm();
			
            BigDecimal marketActionPrice = market.getPrice();

            //BigDecimal lastMarketSessionClearingPrice = null;
            BigDecimal currentReferencePrice = null;
            try {
                //lastMarketSessionClearingPrice = invokeLastMarketSessionClearingPrice(marketId, marketActionFormId);
            	currentReferencePrice = invokeGetCurrentReferencePrice(marketId, marketActionForm, date);

            } catch (IOException e) {

                e.printStackTrace();
            }

            //System.out.println("lastMarketSessionClearingPrice: " + lastMarketSessionClearingPrice.toPlainString());
            System.out.println("currentReferencePrice: " + currentReferencePrice.toPlainString());
            
            //processAction = market.getActionStatusid().getStatus().equals("unchecked");
            ActionStatus myActionStatus;
			try {
				myActionStatus = new Utilities().invokeGetActionStatus(market.getStatusid().intValue());
			
				processAction = myActionStatus.getStatus().equals("unchecked");
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
   
            if (processAction) {

                try {
                    existActorInMarket = invokeMarketActorInMarketplace(marketplaceId, marketActorId);

                } catch (IOException e) {

                    e.printStackTrace();
                }
                
                System.out.println("date: " + date);
                System.out.println("startDelivery: " + startDelivery);
                System.out.println("endDelivery: " + endDelivery);
                System.out.println("startSessionDelivery: " + startSessionDelivery);
                System.out.println("endSessionDelivery: " + endSessionDelivery);
                System.out.println("marketActionForm: " + marketActionForm);
                System.out.println("marketSessionForm: " + marketSessionForm);
                System.out.println("currentReferencePrice: " + currentReferencePrice);
                System.out.println("marketActionPrice: " + marketActionPrice);
                System.out.println("existActorInMarket: " + existActorInMarket);
                
                if (checkBtw(startDelivery, startSessionDelivery, endSessionDelivery) &&
                        checkBtw(endDelivery, startSessionDelivery, endSessionDelivery) &&
                        checkBtw(date, startSession, endSession) &&
                        (marketActionForm.equals(marketSessionForm)) &&
                        //((lastMarketSessionClearingPrice == null) ? true : (marketActionPrice.compareTo(lastMarketSessionClearingPrice) <= 0)) &&
                        ((currentReferencePrice == null) ? true : (marketActionPrice.compareTo(currentReferencePrice) <= 0)) &&
                        (existActorInMarket)) {
                    validAction = true;
                } else {
                    validAction = false;
                }
                ActionStatus statusid = new ActionStatus();
                if (validAction == true) {
                    statusid.setId(2);
                    statusid.setStatus("valid");

                } else {
                    statusid.setId(3);
                    statusid.setStatus("invalid");
                }
//                market.setActionStatusid(statusid);
                market.setStatusid(statusid.getId().intValue());

            }


        }
        try {
            invokeCheckedActionOfMarketSession(actions, marketId, sessionId);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return actions;

    }


    private boolean invokeMarketActorInMarketplace(int marketplaceId, int marketActorId) throws IOException {


        boolean result = false;
        String outputTrue = "\"true\"";
        Properties prop = new Properties();
        prop.load(this.getClass().getClassLoader().getResourceAsStream("config.properties"));

        String informationBrokerServerUrl = prop.getProperty("informationBrokerServerUrl");

        URL url = new URL(informationBrokerServerUrl + "/marketplace/" + marketplaceId + "/marketactor/" + marketActorId + "/exists/");

        System.out.println("invokeMarketActorInMarketplace - Rest url: " + url.toString());
        HttpURLConnection connService = (HttpURLConnection) url.openConnection();
        connService.setDoOutput(true);
        connService.setRequestMethod("GET");
        connService.setRequestProperty("Content-Type", "application/json");
		connService.setRequestProperty("Accept", "application/json");
        log.debug("Authorization: " + Utilities.TOKEN_AUTHORIZATION);
        connService.setRequestProperty("Authorization", Utilities.TOKEN_AUTHORIZATION);

        if (connService.getResponseCode() != HttpURLConnection.HTTP_OK) {
            throw new RuntimeException("Failed : HTTP error code : "
                    + connService.getResponseCode());
        }

        BufferedReader br = new BufferedReader(new InputStreamReader(
                (connService.getInputStream())));

        String output = "";
        System.out.println("Output from Server .... \n");
        while ((output = br.readLine()) != null) {

            System.out.println(output);
            result = output.equals(outputTrue);
        }
        br.close();        
        connService.disconnect();


        return result;
    }


    private BigDecimal invokeGetCurrentReferencePrice(int marketId, String marketActionForm, Date date) throws IOException {


        BigDecimal result = null;
        String outputNull = "null";
        Properties prop = new Properties();
        prop.load(this.getClass().getClassLoader().getResourceAsStream("config.properties"));

//        Calendar calendar = Calendar.getInstance();
//    	calendar.setTime(date);
//    	long myDateMillis = calendar.getTimeInMillis();
    	
        System.out.println("date: " + date);
        
        DateTime time = new DateTime(date);
        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ssZZ");
        String dateISO8601 = fmt.print(time);
        System.out.println("dateISO8601: " + dateISO8601);
        
        String informationBrokerServerUrl = prop.getProperty("informationBrokerServerUrl");

        //URL url = new URL(informationBrokerServerUrl + "/marketplace/" + marketId + "/form/" + marketActionFormId + "/date/" + myDateMillis +"/referencePrice");

        URL url = new URL(informationBrokerServerUrl + "/marketplace/" + marketId + "/form/" + marketActionForm + "/date/" + dateISO8601 +"/referencePriceValue/");
    	
        System.out.println("invokeGetCurrentReferencePrice - Rest url: " + url.toString());
        HttpURLConnection connService = (HttpURLConnection) url.openConnection();
        connService.setDoOutput(true);
        connService.setRequestMethod("GET");
		connService.setRequestProperty("Accept", "application/json");
        connService.setRequestProperty("Content-Type", "application/json");
        log.debug("Authorization: " + Utilities.TOKEN_AUTHORIZATION);
        connService.setRequestProperty("Authorization", Utilities.TOKEN_AUTHORIZATION);

        if (connService.getResponseCode() != HttpURLConnection.HTTP_OK) {
            throw new RuntimeException("Failed : HTTP error code : "
                    + connService.getResponseCode());
        }

        BufferedReader br = new BufferedReader(new InputStreamReader(
                (connService.getInputStream())));

        System.out.println("Output from Server .... \n");

        String output = br.readLine();

        if (!output.equals(outputNull)) {
            result = new BigDecimal(output);
        }
        br.close();
        connService.disconnect();

        return result;
    }
    
    private void invokeCheckedActionOfMarketSession(List<MarketAction> actions, int marketplaceId, int marketSessionid) throws IOException {

        Properties prop = new Properties();

        prop.load(this.getClass().getClassLoader().getResourceAsStream("config.properties"));

        String informationBrokerServerUrl = prop.getProperty("informationBrokerServerUrl");

        URL url = new URL(informationBrokerServerUrl + "/marketplace/" + marketplaceId + "/marketsessions/" + marketSessionid + "/actions/checked/");

        System.out.println("Rest url: " + url.toString());

        HttpURLConnection connService = (HttpURLConnection) url.openConnection();
        connService.setDoOutput(true);
        connService.setRequestMethod("PUT");
		connService.setRequestProperty("Accept", "application/json");
        connService.setRequestProperty("Content-Type", "application/json");
        log.debug("Authorization: " + Utilities.TOKEN_AUTHORIZATION);
        connService.setRequestProperty("Authorization", Utilities.TOKEN_AUTHORIZATION);


        GsonBuilder gb = new GsonBuilder();

        //gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssXX");
        Gson gson = gb.create();

        String record = gson.toJson(actions);

        System.out.println("Rest Request: " + record);

        OutputStream os = connService.getOutputStream();
        os.write(record.getBytes());
        
		int myResponseCode = connService.getResponseCode();
		System.out.println("HTTP error code :" + myResponseCode);
		
        os.flush();
        os.close();
        connService.disconnect();
        
    }

    private boolean checkBtw(Date startDelivery, Date startSessionDelivery, Date endSessionDelivery) {
        boolean ret;
         if ((startDelivery.after(startSessionDelivery) || startDelivery.equals(startSessionDelivery)) &&
                (startDelivery.before(endSessionDelivery) || startDelivery.equals(endSessionDelivery))) {
            ret = true;
        } else {
            ret = false;
        }
        return ret;
    }


}


