package eu.catalyst.marketsession.utilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Properties;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import eu.catalyst.marketsession.model.Form;
import eu.catalyst.marketsession.model.MarketSession;
import eu.catalyst.marketsession.model.ActionStatus;
import eu.catalyst.marketsession.model.MarketActionCounterOffer;
import eu.catalyst.marketsession.model.MarketAction;
import eu.catalyst.marketsession.model.Marketplace;

public class Utilities {
	
	public static String TOKEN_AUTHORIZATION;
	
    public static String now() {
        return ISO8601.now();
    }

    public MarketSession invokeGetMarketSession(int marketSessionId) throws IOException {

		MarketSession result = null;
		String outputNull = "null";
		Properties prop = new Properties();
		prop.load(this.getClass().getClassLoader().getResourceAsStream("config.properties"));

//        Calendar calendar = Calendar.getInstance();
//    	calendar.setTime(date);
//    	long myDateMillis = calendar.getTimeInMillis();

		String informationBrokerServerUrl = prop.getProperty("informationBrokerServerUrl");

		URL url = new URL(informationBrokerServerUrl + "/marketsessions/" + marketSessionId + "/");

		System.out.println("invokeGetMarketSession - Rest url: " + url.toString());
		HttpURLConnection connService = (HttpURLConnection) url.openConnection();
		connService.setDoOutput(true);
		connService.setRequestMethod("GET");
		connService.setRequestProperty("Accept", "application/json");
		connService.setRequestProperty("Content-Type", "application/json");
		connService.setRequestProperty("Authorization", Utilities.TOKEN_AUTHORIZATION);

		if (connService.getResponseCode() != HttpURLConnection.HTTP_OK) {
			throw new RuntimeException("Failed : HTTP error code : " + connService.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
		Type listType = new TypeToken<MarketSession>() {
		}.getType();
		GsonBuilder gb = new GsonBuilder();
		Gson gson = gb.create();
		gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
		result = gson.fromJson(br, listType);
		br.close();
		connService.disconnect();
		return result;
	}
    
    public Marketplace invokeGetMarketplace(int marketplaceid) throws IOException {

    	Marketplace result = null;
		String outputNull = "null";
		Properties prop = new Properties();
		prop.load(this.getClass().getClassLoader().getResourceAsStream("config.properties"));

//        Calendar calendar = Calendar.getInstance();
//    	calendar.setTime(date);
//    	long myDateMillis = calendar.getTimeInMillis();

		String informationBrokerServerUrl = prop.getProperty("informationBrokerServerUrl");

		URL url = new URL(informationBrokerServerUrl + "/marketplace/" + marketplaceid + "/");

		System.out.println("invokeGetMarketplace - Rest url: " + url.toString());
		HttpURLConnection connService = (HttpURLConnection) url.openConnection();
		connService.setDoOutput(true);
		connService.setRequestMethod("GET");
		connService.setRequestProperty("Accept", "application/json");
		connService.setRequestProperty("Content-Type", "application/json");
		connService.setRequestProperty("Authorization", Utilities.TOKEN_AUTHORIZATION);

		if (connService.getResponseCode() != HttpURLConnection.HTTP_OK) {
			throw new RuntimeException("Failed : HTTP error code : " + connService.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
		Type listType = new TypeToken<Marketplace>() {
		}.getType();
		GsonBuilder gb = new GsonBuilder();
		Gson gson = gb.create();
		gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
		result = gson.fromJson(br, listType);
		br.close();
		connService.disconnect();
		return result;
	}

    public MarketAction invokeGetMarketAction(int marketActionId) throws IOException {

    	MarketAction result = null;
		String outputNull = "null";
		Properties prop = new Properties();
		prop.load(this.getClass().getClassLoader().getResourceAsStream("config.properties"));

//        Calendar calendar = Calendar.getInstance();
//    	calendar.setTime(date);
//    	long myDateMillis = calendar.getTimeInMillis();

		String informationBrokerServerUrl = prop.getProperty("informationBrokerServerUrl");

		URL url = new URL(informationBrokerServerUrl + "/marketactions/" + marketActionId + "/");

		System.out.println("invokeGetMarketAction - Rest url: " + url.toString());
		HttpURLConnection connService = (HttpURLConnection) url.openConnection();
		connService.setDoOutput(true);
		connService.setRequestMethod("GET");
		connService.setRequestProperty("Accept", "application/json");
		connService.setRequestProperty("Content-Type", "application/json");
		connService.setRequestProperty("Authorization", Utilities.TOKEN_AUTHORIZATION);

		if (connService.getResponseCode() != HttpURLConnection.HTTP_OK) {
			throw new RuntimeException("Failed : HTTP error code : " + connService.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
		Type listType = new TypeToken<MarketAction>() {
		}.getType();
		GsonBuilder gb = new GsonBuilder();
		Gson gson = gb.create();
		gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
		result = gson.fromJson(br, listType);
		br.close();
		connService.disconnect();
		return result;
	}
    
    public Form invokeGetForm(int formId) throws IOException {

		Form result = null;
		String outputNull = "null";
		Properties prop = new Properties();
		prop.load(this.getClass().getClassLoader().getResourceAsStream("config.properties"));

//        Calendar calendar = Calendar.getInstance();
//    	calendar.setTime(date);
//    	long myDateMillis = calendar.getTimeInMillis();

		String informationBrokerServerUrl = prop.getProperty("informationBrokerServerUrl");

		URL url = new URL(informationBrokerServerUrl + "/form/" + formId + "/");

		System.out.println("invokeGetForm - Rest url: " + url.toString());
		HttpURLConnection connService = (HttpURLConnection) url.openConnection();
		connService.setDoOutput(true);
		connService.setRequestMethod("GET");
		connService.setRequestProperty("Accept", "application/json");
		connService.setRequestProperty("Content-Type", "application/json");
		connService.setRequestProperty("Authorization", Utilities.TOKEN_AUTHORIZATION);

		if (connService.getResponseCode() != HttpURLConnection.HTTP_OK) {
			throw new RuntimeException("Failed : HTTP error code : " + connService.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
		Type listType = new TypeToken<Form>() {
		}.getType();
		GsonBuilder gb = new GsonBuilder();
		Gson gson = gb.create();
		gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
		result = gson.fromJson(br, listType);
		br.close();
		connService.disconnect();
		return result;
	}
    
    public MarketActionCounterOffer invokeGetMarketActionCounterOffer(int marketActionCounterOfferId) throws IOException {

    	MarketActionCounterOffer result = null;
		String outputNull = "null";
		Properties prop = new Properties();
		prop.load(this.getClass().getClassLoader().getResourceAsStream("config.properties"));

//        Calendar calendar = Calendar.getInstance();
//    	calendar.setTime(date);
//    	long myDateMillis = calendar.getTimeInMillis();

		String informationBrokerServerUrl = prop.getProperty("informationBrokerServerUrl");

		URL url = new URL(informationBrokerServerUrl + "/marketactioncounteroffers/" + marketActionCounterOfferId + "/");

		System.out.println("invokeGetMarketActionCounterOffer - Rest url: " + url.toString());
		HttpURLConnection connService = (HttpURLConnection) url.openConnection();
		connService.setDoOutput(true);
		connService.setRequestMethod("GET");
		connService.setRequestProperty("Accept", "application/json");
		connService.setRequestProperty("Content-Type", "application/json");
		connService.setRequestProperty("Authorization", Utilities.TOKEN_AUTHORIZATION);

		if (connService.getResponseCode() != HttpURLConnection.HTTP_OK) {
			throw new RuntimeException("Failed : HTTP error code : " + connService.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
		Type listType = new TypeToken<MarketActionCounterOffer>() {
		}.getType();
		GsonBuilder gb = new GsonBuilder();
		Gson gson = gb.create();
		gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
		result = gson.fromJson(br, listType);
		br.close();
		connService.disconnect();
		return result;
	}
    
    public ActionStatus invokeGetActionStatus(int actionStatusId) throws IOException {

    	ActionStatus result = null;
		String outputNull = "null";
		Properties prop = new Properties();
		prop.load(this.getClass().getClassLoader().getResourceAsStream("config.properties"));

//        Calendar calendar = Calendar.getInstance();
//    	calendar.setTime(date);
//    	long myDateMillis = calendar.getTimeInMillis();

		String informationBrokerServerUrl = prop.getProperty("informationBrokerServerUrl");

		URL url = new URL(informationBrokerServerUrl + "/actionstatus/" + actionStatusId + "/");

		System.out.println("invokeGetActionStatus - Rest url: " + url.toString());
		HttpURLConnection connService = (HttpURLConnection) url.openConnection();
		connService.setDoOutput(true);
		connService.setRequestMethod("GET");
		connService.setRequestProperty("Accept", "application/json");
		connService.setRequestProperty("Content-Type", "application/json");
		connService.setRequestProperty("Authorization", Utilities.TOKEN_AUTHORIZATION);

		if (connService.getResponseCode() != HttpURLConnection.HTTP_OK) {
			throw new RuntimeException("Failed : HTTP error code : " + connService.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
		Type listType = new TypeToken<ActionStatus>() {
		}.getType();
		GsonBuilder gb = new GsonBuilder();
		Gson gson = gb.create();
		gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
		result = gson.fromJson(br, listType);
		br.close();
		connService.disconnect();
		return result;
	}
    
    public static String future(int seconds) {
        long ret = Calendar.getInstance().getTimeInMillis();
        ret += seconds * 1000;
        return dateFromMilliSeconds(ret);
    }

    public static String future(String date, int seconds) {
        Calendar c = Calendar.getInstance();
        try {
            c.setTimeInMillis(ISO8601.toCalendar(date).getTimeInMillis() + seconds * 1000);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return ISO8601.fromCalendar(c);
    }

    public static String past(int seconds) {
        return future(-seconds);
    }

    public static String past(String date, int seconds) {
        return future(date, -seconds);
    }

    public static String dateFromMilliSeconds(long date) {
        String ret = null;
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(date);
        ret = ISO8601.fromCalendar(cal);
        return ret;
    }

    public static String dateFromSeconds(long date) {
        return dateFromMilliSeconds(date * 1000);
    }

    public static long millisFromDateString(String date) {
        long ret = 0;
        try {
            ret = ISO8601.toCalendar(date).getTimeInMillis();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return ret;
    }

    public static long dateDiffMillis(String date1, String date2) {
        long ret = 0;
        try {
            ret = ISO8601.toCalendar(date1).getTimeInMillis() - ISO8601.toCalendar(date2).getTimeInMillis();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return ret;
    }

    public static long secondsFromDateString(String date) {
        return millisFromDateString(date) / 1000;
    }

    public static String fromZ(String datez) {
        String ret = datez;
        try {
            ret = ISO8601.fromCalendar(ISO8601.toCalendar(datez));
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return ret;
    }

    public static final class ISO8601 {
        /**
         * Transform Calendar to ISO 8601 string.
         */
        public static String fromCalendar(final Calendar calendar) {
            Date date = calendar.getTime();
            String formatted = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").format(date);
            return formatted.substring(0, 22) + ":" + formatted.substring(22);
        }

        /**
         * Get current date and time formatted as ISO 8601 string.
         */
        public static String now() {
            return fromCalendar(GregorianCalendar.getInstance());
        }

        /**
         * Transform ISO 8601 string to Calendar.
         */
        public static Calendar toCalendar(final String iso8601string) throws ParseException {
            Calendar calendar = GregorianCalendar.getInstance();
            String s = iso8601string.replace("Z", "+00:00");
            try {
                s = s.substring(0, 22) + s.substring(23);  // to get rid of the ":"
            } catch (IndexOutOfBoundsException e) {
                throw new ParseException("Invalid length", 0);
            }
            Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").parse(s);
            calendar.setTime(date);
            return calendar;
        }
    }
}
