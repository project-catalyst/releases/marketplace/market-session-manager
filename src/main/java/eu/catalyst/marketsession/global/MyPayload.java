package eu.catalyst.marketsession.global;

import java.math.BigDecimal;
import java.util.List;

import eu.catalyst.marketsession.model.MarketAction;
import eu.catalyst.marketsession.model.MarketActionCounterOffer;

public class MyPayload {

	 private List<MarketAction>  bidsOffersActionList;
	 private List<MarketActionCounterOffer> marketActionCounterOfferList;
	 private BigDecimal newClearingPrice;

	public MyPayload() {
	}

	public List<MarketActionCounterOffer> getMarketActionCounterOfferList() {
		return marketActionCounterOfferList;
	}

	public void setMarketActionCounterOfferList(List<MarketActionCounterOffer> marketActionCounterOfferList) {
		this.marketActionCounterOfferList = marketActionCounterOfferList;
	}

	public List<MarketAction> getBidsOffersActionList() {
		return bidsOffersActionList;
	}

	public void setBidsOffersActionList(List<MarketAction> bidsOffersActionList) {
		this.bidsOffersActionList = bidsOffersActionList;
	}

	public BigDecimal getNewClearingPrice() {
		return newClearingPrice;
	}

	public void setNewClearingPrice(BigDecimal newClearingPrice) {
		this.newClearingPrice = newClearingPrice;
	}
}