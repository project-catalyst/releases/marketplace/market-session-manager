package eu.catalyst.marketsession.listener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Properties;
import java.util.TimeZone;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import eu.catalyst.marketsession.quartz.SchedulerJobBillingFlexMarketSession;
import eu.catalyst.marketsession.quartz.SchedulerJobClearFlexMarketSession;
import eu.catalyst.marketsession.quartz.SchedulerJobCompleteAllMarketSession;
import eu.catalyst.marketsession.quartz.SchedulerJobEndAllMarketSession;
import eu.catalyst.marketsession.quartz.SchedulerJobStartAllMarketSession;
import eu.catalyst.marketsession.utilities.Utilities;

public class QuartzSchedulerListener implements ServletContextListener {
	
	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(QuartzSchedulerListener.class);

	private static final long sleepTime = 5000;
	public void contextDestroyed(ServletContextEvent arg0) {
		//
	}

	public void contextInitialized(ServletContextEvent arg0) {


		TimeZone utcTimeZone = TimeZone.getTimeZone("UTC");
        TimeZone.setDefault(utcTimeZone);
        
		// execute Login operation to retrieve Token for next Rest Calls
		executeLogin();

		JobDetail jobStartAllMarketSession = JobBuilder.newJob(SchedulerJobStartAllMarketSession.class)
				.withIdentity("jobStartAllMarketSession", "group1").build();

		JobDetail jobEndAllMarketSession = JobBuilder.newJob(SchedulerJobEndAllMarketSession.class)
				.withIdentity("jobEndAllMarketSession", "group1").build();

		JobDetail jobClearFlexMarketSession = JobBuilder.newJob(SchedulerJobClearFlexMarketSession.class)
				.withIdentity("jobClearFlexMarketSession", "group1").build();

		JobDetail jobCompleteAllMarketSession = JobBuilder.newJob(SchedulerJobCompleteAllMarketSession.class)
				.withIdentity("jobCompleteAllMarketSession", "group1").build();

		JobDetail jobBillingFlexMarketSession = JobBuilder.newJob(SchedulerJobBillingFlexMarketSession.class)
				.withIdentity("jobBillingFlexMarketSession", "group1").build();
		
		try {

			Trigger triggerStartAllMarketSession = TriggerBuilder.newTrigger()
					.withIdentity("triggerStartAllMarketSession", "group1").withSchedule(
							// CronScheduleBuilder.cronSchedule("0/30 * * * * ?"))
							CronScheduleBuilder.cronSchedule("5 * * * * ?"))
					// CronScheduleBuilder.cronSchedule("0 5 * * * ?"))
					.build();

			Trigger triggerEndAllMarketSession = TriggerBuilder.newTrigger()
					.withIdentity("triggerEndAllMarketSession", "group1").withSchedule(
							// CronScheduleBuilder.cronSchedule("0/40 * * * * ?"))
							CronScheduleBuilder.cronSchedule("15 * * * * ?"))
					// CronScheduleBuilder.cronSchedule("0 15 * * * ?"))
					.build();

			Trigger triggerClearFlexMarketSession = TriggerBuilder.newTrigger()
					.withIdentity("triggerClearFlexMarketSession", "group1").withSchedule(
							// CronScheduleBuilder.cronSchedule("0/50 * * * * ?"))
							CronScheduleBuilder.cronSchedule("25  * * * * ?"))
					// CronScheduleBuilder.cronSchedule("0 25 * * * ?"))
					.build();

			Properties prop = new Properties();
			try {
				prop.load(this.getClass().getClassLoader().getResourceAsStream("config.properties"));
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			
			String AllSessionsCompleteTime = prop.getProperty("AllSessionsCompleteTime");
			String AllSessionsCompleteTimeHH = AllSessionsCompleteTime.substring(0, 2);
			String AllSessionsCompleteTimeMM = AllSessionsCompleteTime.substring(3);
			String cronScheduleCompleteAllMarketSession = "0 " + AllSessionsCompleteTimeMM + " "
					+ AllSessionsCompleteTimeHH + " * * ?";
			System.out.println("cronScheduleCompleteAllMarketSession: " + cronScheduleCompleteAllMarketSession);

			Trigger triggerCompleteAllMarketSession = TriggerBuilder.newTrigger()
					.withIdentity("triggerCompleteAllMarketSession", "group1")
					.withSchedule(CronScheduleBuilder.cronSchedule(cronScheduleCompleteAllMarketSession))
					// CronScheduleBuilder.cronSchedule("0 59 23 * * ?"))
					// CronScheduleBuilder.cronSchedule("0 10 10 * * ?"))
					.build();
			
			
			String FlexSessionsBillingTime = prop.getProperty("FlexSessionsBillingTime");
			String FlexSessionsBillingTimeHH = FlexSessionsBillingTime.substring(0, 2);
			String FlexSessionsBillingTimeMM = FlexSessionsBillingTime.substring(3);
			String cronScheduleBillingFlexMarketSession = "0 " + FlexSessionsBillingTimeMM + " "
					+ FlexSessionsBillingTimeHH + " * * ?";
			System.out.println("cronScheduleBillingFlexMarketSession: " + cronScheduleBillingFlexMarketSession);

			Trigger triggerBillingFlexMarketSession = TriggerBuilder.newTrigger()
					.withIdentity("triggerBillingFlexMarketSession", "group1")
					.withSchedule(CronScheduleBuilder.cronSchedule(cronScheduleBillingFlexMarketSession))
					// CronScheduleBuilder.cronSchedule("0 59 23 * * ?"))
					// CronScheduleBuilder.cronSchedule("0 10 10 * * ?"))
					.build();
			
			
			

			Scheduler scheduler = new StdSchedulerFactory().getScheduler();
			scheduler.start();
			scheduler.scheduleJob(jobStartAllMarketSession, triggerStartAllMarketSession);
			scheduler.scheduleJob(jobEndAllMarketSession, triggerEndAllMarketSession);
			scheduler.scheduleJob(jobClearFlexMarketSession, triggerClearFlexMarketSession);
			scheduler.scheduleJob(jobCompleteAllMarketSession, triggerCompleteAllMarketSession);
			scheduler.scheduleJob(jobBillingFlexMarketSession, triggerBillingFlexMarketSession);


		} catch (SchedulerException e) {
			e.printStackTrace();
		}

	}

	private void executeLogin() {

		log.debug("-> executeLogin");

		// load config.properties
		Properties prop = new Properties();
		try {
			prop.load(this.getClass().getClassLoader().getResourceAsStream("config.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}

		// retrieve URL of information broken
		String informationBrokerServerUrl = prop.getProperty("informationBrokerServerUrl");
		log.debug("informationBrokerServerUrl: " + informationBrokerServerUrl);

		// retrieve PATH of Rest Service to call
		String pathPostTokens = prop.getProperty("pathPostTokens");
		log.debug("pathPostTokens: " + pathPostTokens);

		// retrieve USER and PASSWORD for request payload
		String credentialUser = prop.getProperty("credentialUser");
		log.debug("credentialUser: " + credentialUser);
		String credentialPassword = prop.getProperty("credentialPassword");
		log.debug("credentialPassword: " + credentialPassword);

		// create request payload
		JsonObject jsonRequestPayload = new JsonObject();
		jsonRequestPayload.addProperty("username", credentialUser);
		jsonRequestPayload.addProperty("password", credentialPassword);


		// execute Restful Call POST "/tokens/ to informationbroker
		String responsePayload = "";

		// Repeats until the service responds correctly

		boolean repeat_call_service = true;
		
			do {
				try {
				URL url = new URL(informationBrokerServerUrl + pathPostTokens);
				log.debug("Rest url: " + url.toString());

				HttpURLConnection connService = (HttpURLConnection) url.openConnection();
				connService.setDoOutput(true);
				connService.setRequestMethod("POST");
				connService.setRequestProperty("Content-Type", "application/json");
				connService.setRequestProperty("Accept", "application/json");

				String requestPayload = jsonRequestPayload.toString();
				log.debug("requestPayload: " + requestPayload);

				OutputStream os = connService.getOutputStream();
				os.write(requestPayload.getBytes());
				os.flush();

				if (connService.getResponseCode() == HttpURLConnection.HTTP_OK) {
					repeat_call_service = false;
				}

				BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));

				responsePayload = br.readLine();

				log.debug("responsePayload: " + responsePayload);

				br.close();
				connService.disconnect();
				}
			    catch (ConnectException ex) {
				log.error("ConnectException : " + ex);
				repeat_call_service = true;
				try {
					Thread.sleep(sleepTime);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//ex.printStackTrace();
			    }
			    catch (Exception ex) {
					log.error("Exception : " + ex);
					ex.printStackTrace();
				}

			} while (repeat_call_service);
	
//		try {
//			// create url Restful Call POST
//			URL url = new URL(informationBrokerServerUrl + pathPostTokens);
//			log.debug("Rest url: " + url.toString());
//			
//			HttpURLConnection connService = (HttpURLConnection) url.openConnection();
//			connService.setDoOutput(true);
//			connService.setRequestMethod("POST");
//			connService.setRequestProperty("Content-Type", "application/json");
//			connService.setRequestProperty("Accept", "application/json");
//
//			String requestPayload = jsonRequestPayload.toString();
//			log.debug("requestPayload: " + requestPayload);
//
//			OutputStream os = connService.getOutputStream();
//			os.write(requestPayload.getBytes());
//			os.flush();
//
//			if (connService.getResponseCode() != HttpURLConnection.HTTP_OK) {
//				throw new RuntimeException("Failed : HTTP error code : " + connService.getResponseCode());
//			}
//
//			BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
//
//			responsePayload = br.readLine();
//
//			log.debug("responsePayload: " + responsePayload);
//
//			br.close();
//			connService.disconnect();
//
//		} catch (Exception ex) {
//			log.error("Exception : " + ex);
//			ex.printStackTrace();
//		}

		
		
		// parsing JSON Response payload to extract "token" property
		JsonObject responseJson = (JsonObject) new  JsonParser().parse(responsePayload);
		String responseToken = responseJson.get("token").toString().replace("\"", "");
		log.debug("responseToken: " + responseToken);

		// store statically token value for next authentication operations
		Utilities.TOKEN_AUTHORIZATION = "token " + responseToken;
		log.debug("TOKEN_AUTHORIZATION: " + Utilities.TOKEN_AUTHORIZATION);

		log.debug("<- executeLogin");
	}

}