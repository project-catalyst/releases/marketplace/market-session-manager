/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.catalyst.marketsession.model;

import java.io.Serializable;
import java.util.Collection;

public class Form implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;

    private String form;
   
    private Collection<MarketAction> marketActionCollection;

    private Collection<MarketSession> marketSessionCollection;

    public Form() {
    }

    public Form(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getForm() {
        return form;
    }

    public void setForm(String form) {
        this.form = form;
    }


    public Collection<MarketAction> getMarketActionCollection() {
        return marketActionCollection;
    }

    public void setMarketActionCollection(Collection<MarketAction> marketActionCollection) {
        this.marketActionCollection = marketActionCollection;
    }


    public Collection<MarketSession> getMarketSessionCollection() {
        return marketSessionCollection;
    }

    public void setMarketSessionCollection(Collection<MarketSession> marketSessionCollection) {
        this.marketSessionCollection = marketSessionCollection;
    }


    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }


    public boolean equals(Object object) {
        
        if (!(object instanceof Form)) {
            return false;
        }
        Form other = (Form) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eu.catalyst.marketplace.model.Form[ id=" + id + " ]";
    }

}
