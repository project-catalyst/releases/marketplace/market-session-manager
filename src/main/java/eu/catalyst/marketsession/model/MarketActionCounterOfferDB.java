/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.catalyst.marketsession.model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;


@Entity
@Table(name = "MarketActionCounterOffer")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "MarketActionCounterOffer.findAll", query = "SELECT m FROM MarketActionCounterOffer m"),
        @NamedQuery(name = "MarketActionCounterOffer.findById", query = "SELECT m FROM MarketActionCounterOffer m WHERE m.id = :id")})
public class MarketActionCounterOfferDB implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    //Intermediate prototype updates
    //@JoinColumn(name = "MarketAction_Bid_id", referencedColumnName = "id", insertable = false, updatable = false)
    @JoinColumn(name = "MarketAction_Bid_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private MarketAction marketAction_Bid_id;
    //@JoinColumn(name = "MarketAction_Offer_id", referencedColumnName = "id", insertable = false, updatable = false)
    @JoinColumn(name = "MarketAction_Offer_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private MarketAction marketAction_Offer_id;
    @Basic(optional = false)
    @Column(name = "exchangedValue")
//    private long exchangedValue;
    private BigDecimal exchangedValue;
    public MarketActionCounterOfferDB() {
    }

    public MarketActionCounterOfferDB(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    //Intermediate prototype updates
    public MarketAction getMarketAction_Bid_id() {
        return marketAction_Bid_id;
    }

    public void setMarketAction_Bid_id(MarketAction marketAction_Bid_id) {
        this.marketAction_Bid_id = marketAction_Bid_id;
    }

    public MarketAction getMarketAction_Offer_id() {
        return marketAction_Offer_id;
    }

    public void setMarketAction_Offer_id(MarketAction marketAction_Offer_id) {
        this.marketAction_Offer_id = marketAction_Offer_id;
    }

    public BigDecimal getExchangedValue() {
        return exchangedValue;
    }

    public void setExchangedValue(BigDecimal exchangedValue) {
        this.exchangedValue = exchangedValue;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        
        if (!(object instanceof MarketActionCounterOfferDB)) {
            return false;
        }
        MarketActionCounterOfferDB other = (MarketActionCounterOfferDB) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eu.geyser.marketplace.model.MarketActionCounterOffer[ id=" + id + " ]";
    }

}
