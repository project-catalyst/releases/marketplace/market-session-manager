package eu.catalyst.marketsession.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


public class InvoiceDB implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;

    private Date date;

    private BigDecimal penalty;

    private BigDecimal amount;

    private BigDecimal fixedFee;

    private Transaction transactionid;

    public InvoiceDB() {
    }

    public InvoiceDB(Integer id) {
        this.id = id;
    }

    public InvoiceDB(Integer id, Date date) {
        this.id = id;
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getPenalty() {
        return penalty;
    }

    public void setPenalty(BigDecimal penalty) {
        this.penalty = penalty;
    }
    
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    
    public BigDecimal getFixedFee() {
        return fixedFee;
    }

    public void setFixedFee(BigDecimal fixedFee) {
        this.fixedFee = fixedFee;
    }
    
    public Transaction getTransactionid() {
        return transactionid;
    }

    public void setTransactionid(Transaction transactionid) {
        this.transactionid = transactionid;
    }


    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }


    public boolean equals(Object object) {
        
        if (!(object instanceof InvoiceDB)) {
            return false;
        }
        InvoiceDB other = (InvoiceDB) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }


    public String toString() {
        return "eu.catalyst.marketplace.model.Invoice[ id=" + id + " ]";
    }

}
