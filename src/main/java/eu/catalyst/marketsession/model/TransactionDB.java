/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.catalyst.marketsession.model;

import java.io.Serializable;
import java.util.Date;

public class TransactionDB implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;

    private Date dateTime;

    private MarketActionCounterOffer marketActionCounterOfferid;

    public TransactionDB() {
    }

    public TransactionDB(Integer id) {
        this.id = id;
    }

    public TransactionDB(Integer id, Date dateTime) {
        this.id = id;
        this.dateTime = dateTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public MarketActionCounterOffer getMarketActionCounterOfferid() {
        return marketActionCounterOfferid;
    }

    public void setMarketActionCounterOfferid(MarketActionCounterOffer marketActionCounterOfferid) {
        this.marketActionCounterOfferid = marketActionCounterOfferid;
    }


    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }


    public boolean equals(Object object) {
        
        if (!(object instanceof TransactionDB)) {
            return false;
        }
        TransactionDB other = (TransactionDB) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }


    public String toString() {
        return "eu.catalyst.marketplace.model.Transaction[ id=" + id + " ]";
    }

}
