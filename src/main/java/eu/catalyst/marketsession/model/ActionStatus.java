/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.catalyst.marketsession.model;

import java.io.Serializable;
import java.util.Collection;

public class ActionStatus implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;
    private String status;
    private Collection<MarketAction> marketActionCollection;

    public ActionStatus() {
    }

    public ActionStatus(Integer id) {
        this.id = id;
    }

    public ActionStatus(Integer id, String status) {
        this.id = id;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public Collection<MarketAction> getMarketActionCollection() {
        return marketActionCollection;
    }

    public void setMarketActionCollection(Collection<MarketAction> marketActionCollection) {
        this.marketActionCollection = marketActionCollection;
    }


    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    public boolean equals(Object object) {
        
        if (!(object instanceof ActionStatus)) {
            return false;
        }
        ActionStatus other = (ActionStatus) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }


    public String toString() {
        return "eu.catalyst.marketplace.model.ActionStatus[ id=" + id + " ]";
    }

}
