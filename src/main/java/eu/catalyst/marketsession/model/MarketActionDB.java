/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.catalyst.marketsession.model;

import eu.catalyst.marketsession.global.DateDeSerializer;
import eu.catalyst.marketsession.global.DateSerializer;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;


@Entity
@Table(name = "MarketAction")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "MarketAction.findAll", query = "SELECT m FROM MarketAction m"),
        @NamedQuery(name = "MarketAction.findById", query = "SELECT m FROM MarketAction m WHERE m.id = :id"),
        @NamedQuery(name = "MarketAction.findByDate", query = "SELECT m FROM MarketAction m WHERE m.date = :date"),
        @NamedQuery(name = "MarketAction.findByActionStartTime", query = "SELECT m FROM MarketAction m WHERE m.actionStartTime = :actionStartTime"),
        @NamedQuery(name = "MarketAction.findByActionEndTime", query = "SELECT m FROM MarketAction m WHERE m.actionEndTime = :actionEndTime"),
        @NamedQuery(name = "MarketAction.findByValue", query = "SELECT m FROM MarketAction m WHERE m.value = :value"),
        @NamedQuery(name = "MarketAction.findByUom", query = "SELECT m FROM MarketAction m WHERE m.uom = :uom"),
        @NamedQuery(name = "MarketAction.findByPrice", query = "SELECT m FROM MarketAction m WHERE m.price = :price"),
        @NamedQuery(name = "MarketAction.findByDeliveryPoint", query = "SELECT m FROM MarketAction m WHERE m.deliveryPoint = :deliveryPoint")})
public class MarketActionDB implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonDeserialize(using = DateDeSerializer.class)
    @JsonSerialize(using = DateSerializer.class)
    private Date date;
    @Basic(optional = false)
    @NotNull
    @Column(name = "actionStartTime")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonDeserialize(using = DateDeSerializer.class)
    @JsonSerialize(using = DateSerializer.class)
    private Date actionStartTime;
    @Basic(optional = false)
    @NotNull
    @Column(name = "actionEndTime")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonDeserialize(using = DateDeSerializer.class)
    @JsonSerialize(using = DateSerializer.class)
    private Date actionEndTime;
    @Basic(optional = false)
    @NotNull
    @Column(name = "value")
    //private long value;
    private BigDecimal value;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "uom")
    private String uom;
    @Basic(optional = false)
    @NotNull
    @Column(name = "price")
    private BigDecimal price;
    @Size(max = 45)
    @Column(name = "deliveryPoint")
    private String deliveryPoint;
    @JoinColumn(name = "ActionStatus_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private ActionStatus actionStatusid;
    @JoinColumn(name = "MarketSession_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private MarketSession marketSessionid;
    @JoinColumn(name = "MarketActor_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private MarketActor marketActorid;
    @JoinColumn(name = "Form_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Form formid;
    @JoinColumn(name = "ActionType_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private ActionType actionTypeid;
    //Intermediate prototype updates
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "marketAction_Bid_id")
    private Collection<MarketActionCounterOffer> marketActionCounterOfferBidCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "marketAction_Offer_id")
    private Collection<MarketActionCounterOffer> marketActionCounterOfferOfferCollection;

    public MarketActionDB() {
    }

    public MarketActionDB(Integer id) {
        this.id = id;
    }

    public MarketActionDB(Integer id, Date date, Date actionStartTime, Date actionEndTime, BigDecimal value, String uom, BigDecimal price) {
        this.id = id;
        this.date = date;
        this.actionStartTime = actionStartTime;
        this.actionEndTime = actionEndTime;
        this.value = value;
        this.uom = uom;
        this.price = price;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getActionStartTime() {
        return actionStartTime;
    }

    public void setActionStartTime(Date actionStartTime) {
        this.actionStartTime = actionStartTime;
    }

    public Date getActionEndTime() {
        return actionEndTime;
    }

    public void setActionEndTime(Date actionEndTime) {
        this.actionEndTime = actionEndTime;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getDeliveryPoint() {
        return deliveryPoint;
    }

    public void setDeliveryPoint(String deliveryPoint) {
        this.deliveryPoint = deliveryPoint;
    }


    public ActionStatus getActionStatusid() {
        return actionStatusid;
    }

    public void setActionStatusid(ActionStatus statusid) {
        this.actionStatusid = statusid;
    }

    public MarketSession getMarketSessionid() {
        return marketSessionid;
    }

    public void setMarketSessionid(MarketSession marketSessionid) {
        this.marketSessionid = marketSessionid;
    }

    public MarketActor getMarketActorid() {
        return marketActorid;
    }

    public void setMarketActorid(MarketActor marketActorid) {
        this.marketActorid = marketActorid;
    }

    public Form getFormid() {
        return formid;
    }

    public void setFormid(Form formid) {
        this.formid = formid;
    }

    public ActionType getActionTypeid() {
        return actionTypeid;
    }

    public void setActionTypeid(ActionType actionTypeid) {
        this.actionTypeid = actionTypeid;
    }

    //Intermediate prototype updates
    @XmlTransient
    @JsonIgnore
    public Collection<MarketActionCounterOffer> getMarketActionCounterOfferBidCollection() {
        return marketActionCounterOfferBidCollection;
    }

    public void setMarketActionCounterOfferBidCollection(Collection<MarketActionCounterOffer> marketActionCounterOfferBidCollection) {
        this.marketActionCounterOfferBidCollection = marketActionCounterOfferBidCollection;
    }

    @XmlTransient
    @JsonIgnore
    public Collection<MarketActionCounterOffer> getMarketActionCounterOfferOfferCollection() {
        return marketActionCounterOfferOfferCollection;
    }

    public void setMarketActionCounterOfferOfferCollection(Collection<MarketActionCounterOffer> marketActionCounterOfferOfferCollection) {
        this.marketActionCounterOfferOfferCollection = marketActionCounterOfferOfferCollection;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        
        if (!(object instanceof MarketActionDB)) {
            return false;
        }
        MarketActionDB other = (MarketActionDB) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eu.geyser.marketplace.model.MarketAction[ id=" + id + " ]";
    }

}
