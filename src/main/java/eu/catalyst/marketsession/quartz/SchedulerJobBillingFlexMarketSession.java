package eu.catalyst.marketsession.quartz;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.util.TimeZone;

import eu.catalyst.marketsession.global.MyPayload;
import eu.catalyst.marketsession.model.Form;
import eu.catalyst.marketsession.model.MarketAction;
import eu.catalyst.marketsession.model.MarketSession;
import eu.catalyst.marketsession.utilities.Utilities;

public class SchedulerJobBillingFlexMarketSession implements Job {
	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger
			.getLogger(SchedulerJobBillingFlexMarketSession.class);

	public void execute(JobExecutionContext context) throws JobExecutionException {

		Properties prop = new Properties();
		// load properties from the class path
		ArrayList<MarketAction> returnMarketActionNotBilledList = new ArrayList<MarketAction>();

		try {
			prop.load(this.getClass().getClassLoader().getResourceAsStream("config.properties"));

			String informationBrokerServerUrl = prop.getProperty("informationBrokerServerUrl");

			// URL url = new URL(informationBrokerServerUrl +
			// "/marketplace/1/marketsessions/cleared/offers/notbilled/");
			//URL url = new URL(informationBrokerServerUrl + "/marketplace/gam/marketsessions/cleared/offers/notbilled/");
			URL url = new URL(informationBrokerServerUrl + "/marketplace/gam/marketsessions/completed/offers/notbilled/");

			log.debug("Start - Rest url: " + url.toString());
			HttpURLConnection connService = (HttpURLConnection) url.openConnection();

			connService.setDoOutput(true);
			connService.setRequestMethod("GET");
			connService.setRequestProperty("Content-Type", "application/json");
			connService.setRequestProperty("Accept", "application/json");
			log.debug("Authorization: " + Utilities.TOKEN_AUTHORIZATION);
			connService.setRequestProperty("Authorization", Utilities.TOKEN_AUTHORIZATION);

			if (connService.getResponseCode() != HttpURLConnection.HTTP_OK
					&& connService.getResponseCode() != HttpURLConnection.HTTP_NO_CONTENT) {
				throw new RuntimeException("Failed : HTTP error code : " + connService.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
			Type listType = new TypeToken<ArrayList<MarketAction>>() {
			}.getType();
			GsonBuilder gb = new GsonBuilder();
			gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
			Gson gson = gb.create();
			returnMarketActionNotBilledList = gson.fromJson(br, listType);
			br.close();
			connService.disconnect();

			if (!returnMarketActionNotBilledList.isEmpty()) {

				eu.catalyst.marketsession.global.MyPayload myPayload = new MyPayload();
				myPayload.setMarketActionCounterOfferList(null);
				myPayload.setBidsOffersActionList(returnMarketActionNotBilledList);
				myPayload.setNewClearingPrice(null);

				// chiamare un servizio che ritorni la MarketSession dato l'id
				MarketSession myMarketSession = new Utilities().invokeGetMarketSession(
						returnMarketActionNotBilledList.get(0).getMarketSessionid().intValue());

				// int marketId =
				// returnMarketActionNotBilledList.get(0).getMarketSessionid().getMarketplaceid().getId().intValue();
				//int marketId = myMarketSession.getMarketplaceid().getId().intValue();
				int marketId = myMarketSession.getMarketplaceid().intValue();

				String marketSessionManagerUrl = prop.getProperty("marketSessionManagerUrl");

				// chiamare un Servizio che ritorni la Form dato l'id
				// String form = returnMarketActionNotBilledList.get(0).getFormid().getForm();
				Form myForm = new Utilities().invokeGetForm(returnMarketActionNotBilledList.get(0).getFormid().intValue());
				String form = myForm.getForm();

				// int sessionId =
				// returnMarketActionNotBilledList.get(0).getMarketSessionid().getId().intValue();
				int sessionId = returnMarketActionNotBilledList.get(0).getMarketSessionid().intValue();

				URL url2 = new URL(marketSessionManagerUrl + "/marketplace/" + marketId + "/form/"
						+ form + "/marketsessions/" + sessionId + "/billing/actions/");

				System.out.println("Rest url2: " + url2.toString());

				HttpURLConnection connService2 = (HttpURLConnection) url2.openConnection();
				connService2.setDoOutput(true);
				connService2.setRequestMethod("POST");
				connService2.setRequestProperty("Accept", "application/json");
				connService2.setRequestProperty("Content-Type", "application/json");
				GsonBuilder gb2 = new GsonBuilder();

//				TimeZone utcTimeZone = TimeZone.getTimeZone("UTC");
//		        TimeZone.setDefault(utcTimeZone);
		        
				//gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				gb2.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
				Gson gson2 = gb2.create();
				

				String record2 = gson2.toJson(myPayload);

				System.out.println("Rest Request: " + record2);

				OutputStream os2 = connService2.getOutputStream();
				os2.write(record2.getBytes());

				int myResponseCode2 = connService2.getResponseCode();
				System.out.println("HTTP error code :" + myResponseCode2);

				os2.flush();
				os2.close();
				connService2.disconnect();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
