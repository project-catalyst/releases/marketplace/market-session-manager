package eu.catalyst.marketsession.quartz;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.google.gson.JsonObject;

import eu.catalyst.marketsession.listener.QuartzSchedulerListener;
import eu.catalyst.marketsession.utilities.Utilities;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Statement;
import java.util.Properties;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class SchedulerJobStartAllMarketSession implements Job {
	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger
			.getLogger(SchedulerJobStartAllMarketSession.class);

	public void execute(JobExecutionContext context) throws JobExecutionException {

		Properties prop = new Properties();
		// load properties from the class path

		Statement st = null;
		try {
			prop.load(this.getClass().getClassLoader().getResourceAsStream("config.properties"));

			String informationBrokerServerUrl = prop.getProperty("informationBrokerServerUrl");

			URL url = new URL(informationBrokerServerUrl + "/marketplace/all/update/marketsessions/active/");

			log.debug("Start - Rest url: " + url.toString());
			HttpURLConnection connService = (HttpURLConnection) url.openConnection();
			connService.setDoOutput(false);
			connService.setRequestMethod("PUT");
			connService.setRequestProperty("Content-Type", "application/json");
			connService.setRequestProperty("Accept", "application/json");
			log.debug("Authorization: " + Utilities.TOKEN_AUTHORIZATION);
			connService.setRequestProperty("Authorization", Utilities.TOKEN_AUTHORIZATION);

			// -----------------------------------------------------------------------
//			JsonObject jsonRequestPayload = new JsonObject();
//			jsonRequestPayload.addProperty("sessionStartTime", "2019-04-12T09:40:57.924Z");
//			jsonRequestPayload.addProperty("sessionEndTime", "2019-04-12T09:40:57.924Z");
//			jsonRequestPayload.addProperty("deliveryStartTime", "2019-04-12T09:40:57.924Z");
//			jsonRequestPayload.addProperty("deliveryEndTime", "2019-04-12T09:40:57.924Z");
//			jsonRequestPayload.addProperty("clearingPrice", 0);
//			String requestPayload = jsonRequestPayload.toString();
//			log.debug("requestPayload: " + requestPayload);
//
//			OutputStream os = connService.getOutputStream();
//			os.write(requestPayload.getBytes());
//			os.flush();
			// -----------------------------------------------------------------------

			if (connService.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + connService.getResponseCode());
            }
//				log.debug("Failed : HTTP error code : " + connService.getResponseCode());
//				try {
//					FileWriter fileWriter = null;
//					File file = new File("/work/Oracle/apache-tomcat-9.0.8/webapps/marketsession/error.html");
//					BufferedWriter bufferedWriter = null;
//					fileWriter = new FileWriter(file);
//					bufferedWriter = new BufferedWriter(fileWriter);
//
//					String thisLine = null;
//					BufferedReader br = new BufferedReader(new InputStreamReader((connService.getErrorStream())));
//
//					while ((thisLine = br.readLine()) != null) {
//						// log.debug(thisLine);
//						bufferedWriter.write(thisLine);
//					}
//					br.close();
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}

//            if (connService.getResponseCode() != HttpURLConnection.HTTP_NO_CONTENT) {
//                throw new RuntimeException("Failed : HTTP error code : "
//                        + connService.getResponseCode());
//            }

			connService.disconnect();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}