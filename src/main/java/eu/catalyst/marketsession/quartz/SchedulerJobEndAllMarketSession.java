package eu.catalyst.marketsession.quartz;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import eu.catalyst.marketsession.utilities.Utilities;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Statement;
import java.util.Properties;

public class SchedulerJobEndAllMarketSession implements Job {
	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SchedulerJobEndAllMarketSession.class);
    public void execute(JobExecutionContext context)
            throws JobExecutionException {

        Properties prop = new Properties();
        //load properties from the class path

        Statement st = null;
        try {
            prop.load(this.getClass().getClassLoader().getResourceAsStream("config.properties"));

            String informationBrokerServerUrl = prop.getProperty("informationBrokerServerUrl");

            URL url = new URL(informationBrokerServerUrl + "/marketplace/all/update/marketsessions/closed/");

            log.debug("Start - Rest url: " + url.toString());
            HttpURLConnection connService = (HttpURLConnection) url.openConnection();
            connService.setDoOutput(false);
            connService.setRequestMethod("PUT");
            connService.setRequestProperty("Content-Type", "application/json");
            connService.setRequestProperty("Accept", "application/json");
            log.debug("Authorization: " + Utilities.TOKEN_AUTHORIZATION);
            connService.setRequestProperty("Authorization", Utilities.TOKEN_AUTHORIZATION);

            if (connService.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + connService.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (connService.getInputStream())));
            br.close();
            connService.disconnect();
            
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
